// light
// author: Ondrej Sika, http://ondrejsika.com
 
var http = require('http');

exports.LightServer = function(routes){
    return http.createServer(function (req, res) {
        url = req.url.split("?")[0];
        if (url[url.length-1] != "/") url = url+"/";
        if (routes[url]){
            routes[url](req, function(data, mime){
                res.writeHead(200, {'Content-Type': mime});
                res.end(data);
            });
        }
        else {
            res.writeHead(404, {'Content-Type': "text/plain"});
            res.end("404");
        }
    });
}
